extends Node

var decks = 2 # total number of decks (max 8)
var suits = 4 # number of suits in each deck (max 4)
var cards = 13 # number of cards for each suit (max 13)

var Deck_Names  = ["First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth"]
var Suit_Names  = ["Spades", "Hearts", "Clubs", "Diamonds"]
var Value_Names = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"]

var deck = []
var colliding_highest_z = decks * 1000

var number_of_tableaus = 10
var cards_on_tableau = []

func _ready():
	
	var deck_by_name
	var suit_by_name
	var value_by_name
	var card_count = 0

	for i in range(decks):
		deck_by_name = Deck_Names[i]

		for j in range(suits):
			suit_by_name = Suit_Names[j]

			for k in range(cards):
				value_by_name = Value_Names[k]

				var card_info = {
					'uid': card_count,
					'value': k + 1,
					'suit': j,
					'location': null,
					'tableau_index': -1,
					'tableau_number': -1,
					'text': {'value': value_by_name,
							 'suit': suit_by_name,
							 'deck': deck_by_name} }
				card_count += 1
				deck.push_back(card_info)

	randomize()
	deck.shuffle()
	
	var current_stock = 0
	var max_stock = number_of_tableaus * 5 # should be a multiple of number of tableaus
	
	for t in range(number_of_tableaus):
		cards_on_tableau.push_back(0)
	
	# stack of cards in top left corner
	for index in deck.size():
		
		var new_scene = preload("res://Card.tscn").instance()
		new_scene.set_name("card_id_" + str(index))
		new_scene.data = deck[index]
		new_scene.z_index = index
		new_scene.get_child(0).z_index = index
		new_scene.get_child(1).z_index = index
		
		# add a card to teh stock piles
		if current_stock < max_stock:
			var s = get_child(10) # Stock_1 index
			new_scene.global_position = Vector2(s.global_position.x, s.global_position.y)
			new_scene.flip_card()
			new_scene.data.location = 'stock'

			current_stock += 1

		# add a card to the tableaus
		else:
			var tableau = index % number_of_tableaus
			var t = get_child(tableau)
			new_scene.global_position = Vector2(t.global_position.x, t.global_position.y + (cards_on_tableau[tableau] * 32))
			new_scene.data.location = 'tableau_' + str(tableau + 1)
			new_scene.data.tableau_index = tableau
			new_scene.data.tableau_number = cards_on_tableau[tableau] + 1

			if (index - max_stock) >= 10: # number of tableaus
				var previous = (index - 10)
				var undercard = get_node("/root/Table/card_id_" + str(previous))
				undercard.flip_card()

			cards_on_tableau[tableau] += 1
			
			
			
		
		add_child(new_scene)
		
		


